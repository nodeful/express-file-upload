const express = require('express')
const server = express()
const fileUpload = require('express-fileupload')
const bodyParser = require('body-parser')
const path = require('path')
const port = process.env.PORT || 8080
const http = require('http')

server.use(bodyParser.json())
server.use(fileUpload())

server.use(express.static(path.join(__dirname, 'public')))

server.post('/json', (req, res) => {
  console.log(req.body)
  res.json(req.body)
})

server.post('/upload', (req, res) => {
  console.log(req.files)
  res.end()
})

http.createServer(server).listen(port)
console.log(`Listening on port: ${port}`)
